/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

/**
 *
 * @author aino
 */
public class PostData {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String automat;
    private String lat;
    private String lng;

    public PostData(String i, String c, String ad, String av, String a, String n, String s){
        code = i;
        city = c;
        address = ad;
        availability = av;
        automat = a;
        lat = n;
        lng = s;
    }

    public String getAddress(){
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAutomat() {
        return automat;
    }

    public String getLatitude() {
        return lat;
    }

    public String getLongitude() {
        return lng;
    }
}