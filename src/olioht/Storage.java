/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

import java.util.ArrayList;

public class Storage {
    private String startAutomat;
    private String endAutomat;
    private ArrayList<TIMOPackage> list = new ArrayList<>();
    private final static Storage s = new Storage();
    
    private Storage(){
    }
    
  static public Storage getInstance() {
      return s;
  }
  
  public void add(TIMOPackage p){
      list.add(p);
  }
  
  public ArrayList<TIMOPackage> getPackageList(){
      return list;
  }
}