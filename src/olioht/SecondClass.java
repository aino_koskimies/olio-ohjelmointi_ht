/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

public class SecondClass extends TIMOPackage {

  public SecondClass (item i, String s, String e) {
      super (i, s, e);
      contents = i;
      startAutomat = s;
      endAutomat = e;
      System.out.println("debug 2. luokka");
  }

    @Override
    public item getItem() {
        return contents;
    }

    @Override
    public String getStartAutomat() {
        return startAutomat;
    }

    @Override
    public String getEndAutomat() {
        return endAutomat;
    }
}