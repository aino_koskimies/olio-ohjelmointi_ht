/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author aino
 */
public class FXMLClassInfoController implements Initializable {
    @FXML
    private Label infoLabel;
    @FXML
    private Button closeButton;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //add info text to the window
        infoLabel.setText("1. luokan paketti on kaikista nopein pakettiluokka," +
                " jonka vuoksi sitä ei voi lähettää pidemmälle kuin 150 km päähän."+
                " Onhan yleisesti tiedossa, että sen pidempää matkaa ei TIMO-mies "+
                "jaksa pakettia kuljettaa. 1. luokan paketti on myös nopea, koska "+
                "sen turvallisuudesta ei välitetä niin paljon, jolloin kaikki "+
                "särkyvät esineet tulevat menemään rikki matkan aikana. \n" +
                "1. luokan pakettien maksimipaino on 5 kg, jotta TIMO-mies ei "+
                "uuvahda matkalla."+
                " \n" +
                "\n" +
                "2. luokan paketit ovat turvakuljetuksia, jolloin ne kestävät "+
                "parhaiten kaiken särkyvän tavaran kuljettamisen. Näitä paketteja "+
                "on mahdollista kuljettaa jopa Lapista Helsinkiin, sillä matkan "+
                "aikana käytetään useampaa kuin yhtä TIMO-miestä, jolloin "+
                "turvallinen kuljetus on taattu. Paketissa on kuitenkin huomattava, "+
                "että jos se on liian suuri, ei särkyvä esine voi olla heilumatta, "+
                "tämän vuoksi paketin maksimimitat ovat 30cm*30cm*30cm."+
                "\n" +
                " \n" +
                "\n" +
                "3. luokan paketti on TIMO-miehen stressinpurkupaketti. Tämä "+
                "tarkoittaa sitä, että TIMO-miehellä ollessa huono päivä pakettia "+
                "paiskotaan seinien kautta automaatista toiseen, joten paketin "+
                "sisällön on oltava myös erityisen kestävää materiaalia. Myös "+
                "esineen suuri koko ja paino ovat eduksi, jolloin TIMO-mies ei "+
                "jaksa heittää pakettia seinälle kovin montaa kertaa. Suosittelemmekin "+
                "että lähetettävä paketti on mitoiltaan yli 50cm*50cm*50cm ja painoltaan "+
                "yli 20 kg. Alle 5 kg pakettia ei voi lähettää 3. luokassa. Koska paketit "+
                "päätyvät kohteeseensa aina seinien kautta, on tämä hitain "+
                "mahdollinen kuljetusmuoto paketille. ");
    }    

    @FXML
    private void closeWindow(ActionEvent event) {
        // makes a handle
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // closes the view
        stage.close();
    }
}