/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author aino
 */
public class DataFetcher {
    
    private Document doc;
    private ArrayList<PostData> list;
    private static DataFetcher df = null;
    PostData pd;
    
    private DataFetcher(String content, String value){
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            list = new ArrayList();
            parseData(value);
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataFetcher.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("DataFetcher error");
        }
    }
    
    static public DataFetcher getInstance(String content, String value){
    if(df == null){    
        df = new DataFetcher(content, value);
    }
        return df;
    }
    
    private void parseData(String value){
        NodeList nodes = doc.getElementsByTagName("place"); //what node we want

        for(int i=0; i<nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            String city = getValue("city", e);
            String number = getValue("code", e);
            String automat = getValue("postoffice", e);
            String address = getValue("address", e);
            String open = getValue("availability", e);
            String lat = getValue("lat", e);
            String lng = getValue("lng", e);
            String data = getValue(value, e);
            
            //adds data to the list, where we can fetch it later
            list.add(new PostData(number, city, address, open, automat, lat, lng));
        }
    }
    //value and where it is
    private String getValue(String tag, Element e){
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }

    public ArrayList<PostData> getList(){
        return list;
    }   
}