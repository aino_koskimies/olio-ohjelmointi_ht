/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;

/**
 *
 * @author aino
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private WebView web;
    @FXML
    private Button drawRoadButton;
    @FXML
    private ComboBox<String> City;
    @FXML
    private Button clear;
    @FXML
    private ComboBox<TIMOPackage> storageBox;
    @FXML
    private Button sendButton;
    @FXML
    private Button createPackage;
    @FXML
    private Button refreshStorage;
    @FXML
    private Label errorLabel;
    
    private DataFetcher df;
    Storage storage = Storage.getInstance();
    ArrayList<TIMOPackage> list = new ArrayList<>();
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //open the map, notice that you can move the map with arrow keys only
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        try {
            //load XML
            URL urli = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(urli.openStream()));
            
            String content = "";
            String line;
            
            //read XML
            while((line = br.readLine()) != null){
                content += line + "\n";
            }

            //get cities
            df = DataFetcher.getInstance(content, "postoffice");
            
            //add cities to the combobox
            int combocount=0; //number of cities in combobox
            boolean match = false;
            for(int i = 0; i < df.getList().size(); i++){
                String city = df.getList().get(i).getCity();
                
                //if adding the first item
                if(combocount == 0){
                    City.getItems().add(city);
                    combocount++;
                }
                else{
                    //go through the combobox
                    for(int j = 0; j < combocount; j++){
                        //check for duplicates
                        String last = City.getItems().get(j);
                        if(city.equals(last)){
                            match = true;
                            break;
                        }
                    }
                    //if no duplicates found, add city to the combobox
                    if(match == false){
                        City.getItems().add(city);
                        combocount++;
                    }
                    match = false;
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            errorLabel.setText("DataFetcher returned value: " + df);
        }
    }
 
    @FXML
    private void createPackageAction(ActionEvent event) {
        openNewView view = new openNewView("FXMLAddPackage.fxml");
    }
    
    @FXML
    private void drawLocations(ActionEvent event) {
        String address;
        String postcode;
        String city;
        String open;
        
        try{
        String start = City.getValue();
        
            //draw all postoffices
            for(int i=0; i<df.getList().size();i++){
                //when wanted city is found
                if(start.equals(df.getList().get(i).getCity())){
                    address = df.getList().get(i).getAddress();
                    postcode = df.getList().get(i).getCode();
                    city = df.getList().get(i).getCity();
                    open = df.getList().get(i).getAvailability();

                    //draw a smartPost location
                    web.getEngine().executeScript("document.goToLocation('"+address+", "+postcode+" "+city+"','Auki: "+open+"','blue')");
                }
            }
        }
        catch(NullPointerException e){
            errorLabel.setText("Et valinnut kaupunkia.");
        }
    }

    @FXML
    private void clearRoads(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void sendPackage(ActionEvent event) {
        String slat="", slng="", elat="", elng="";
        TIMOPackage tp;
        String endAutomat, startAutomat;
        ArrayList<String> list = new ArrayList<>();
        errorLabel.setText(null);
        double pathLength;
        
        tp = storageBox.getValue();
        //error detection
        if(tp == null){
            errorLabel.setText("Valitse listalta arvo tai luo uusi paketti ja päivitä lista.");
            return;
        }
        
        startAutomat = tp.getStartAutomat();
        endAutomat = tp.getEndAutomat();
        
        //find the right automat
        for(int i = 0; i < df.getList().size(); i++){
            //save cordinates
            if(startAutomat.equals(df.getList().get(i).getAutomat())){
                slat = df.getList().get(i).getLatitude();
                slng = df.getList().get(i).getLongitude();
            }
            if(endAutomat.equals(df.getList().get(i).getAutomat())){
                elat = df.getList().get(i).getLatitude();
                elng = df.getList().get(i).getLongitude();
            }
        }
        //error detection
        if(slat.equals("") || slng.equals("") || elat.equals("") || elng.equals("")){
            System.out.println("Koordinaattien haku epäonnistui.");
            return;
        }
        
        //start cordinates:
        list.add(slat);
        list.add(slng);
        
        //end cordinates:
        list.add(elat);
        list.add(elng);
        
        String obj = tp.toString().substring(7, 8);
        
        //check the class and draw the road at certain speed
        if(obj.equals("F")){ //FirstClass
            pathLength = getPathLength(slat, slng, elat, elng);
            if(pathLength > 150){
                errorLabel.setText("Matka yli 150km, paketti katosi matkalla.");
                return;
            }
            else{
                web.getEngine().executeScript("document.createPath("+list+", 'red', 0.2)");
            }
        }
        else if(obj.equals("S")){ //SecondClass
            web.getEngine().executeScript("document.createPath("+list+", 'red', 0.7)");
        }
        else if(obj.equals("T")){ //ThirdClass
            web.getEngine().executeScript("document.createPath("+list+", 'red', 1.2)");
        }
        else{
            errorLabel.setText("Virhe postissa.");
        }
    }
    
    public double getPathLength(String sLat, String sLng, String eLat, String eLng){
        double pathLength;
        ArrayList<String> list = new ArrayList<>();
        
        //start cordinates
        list.add(sLat);
        list.add(sLng);
        
        //end cordinates
        list.add(eLat);
        list.add(eLng);
        
        //this draws the road with blue color, so if the package is lost, the road is shown with blue color
        pathLength = (Double) web.getEngine().executeScript("document.createPath("+list+", 'blue', 0)");
        return pathLength;
    }

    @FXML
    private void refreshStorageAction(ActionEvent event) {
        //get packagelist from storage
        list = storage.getPackageList();
        storageBox.getItems().clear();
        
        //add packages to the combobox
        for(TIMOPackage p : list){
            storageBox.getItems().add(p);
        }
    }
}