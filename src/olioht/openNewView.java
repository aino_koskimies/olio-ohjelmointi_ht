/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author aino
 */
public class openNewView {
    
    public openNewView(String s){
        //takes in the name of the wanted FXML document
        getView(s);
    }
    
    public void getView(String s){
        System.out.println(s);
        //open new FXML window
        try {
            Stage box = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource(s));
            Scene scene = new Scene(page);
            
            box.setScene(scene);
            box.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}