/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

/**
 *
 * @author aino
 */
public class item {
  private String name;
  private boolean fragile;
  private double weight, height, width, depth;
  
  public item(String n, boolean f, double we, double h, double wi, double d){
      name = n;
      fragile = f;
      weight = we;
      height = h;
      width = wi;
      depth = d;
  }

    public String getName() {
        return name;
    }

    public boolean isFragile() {
        return fragile;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public double getDepth() {
        return depth;
    }
}