/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

/**
 *
 * @author aino
 */
public abstract class TIMOPackage {

    protected String startAutomat;
    protected String endAutomat;
    protected item contents;
    
    public TIMOPackage(item i, String s, String e){
        contents = i;
        startAutomat = s;
        endAutomat = e;
    }

    public abstract item getItem();
    public abstract String getStartAutomat();
    public abstract String getEndAutomat();
}