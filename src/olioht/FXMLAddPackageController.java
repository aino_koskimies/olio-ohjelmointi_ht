/*
Aino Koskimies
0437676

December 2015
 */
package olioht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author aino
 */
public class FXMLAddPackageController implements Initializable {
    @FXML
    private Font x1;
    @FXML
    private RadioButton RadioButton1;
    @FXML
    private RadioButton RadioButton2;
    @FXML
    private RadioButton RadioButton3;
    @FXML
    private ToggleGroup ButtonGroup;
    @FXML
    private Font x2;
    @FXML
    private Button infoButton;
    @FXML
    private Label errorLabel;
    @FXML
    private ComboBox<String> startCity;
    @FXML
    private ComboBox<String> endCity;
    @FXML
    private ComboBox<String> items;
    @FXML
    private ComboBox<String> startAutomat;
    @FXML
    private ComboBox<String> endAutomat;
    @FXML
    private CheckBox fragileBox;
    @FXML
    private Button createPackageButton;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private Button closeButton;

    private DataFetcher df;
    ArrayList<item> itemList = new ArrayList<>();
    Storage storage = Storage.getInstance();
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // get smartpost locations
        try {
            URL urli = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(urli.openStream()));
            String content = "";
            String line;
            
            //read XML
            while((line = br.readLine()) != null){
                content += line + "\n";
            }
            //get cities
            df = DataFetcher.getInstance(content, "postoffice");
            
            //add cities to comboboxes
            int combocount = 0; //how many cities in combobox
            boolean match = false;
            for(int i = 0; i < df.getList().size(); i++){
                String city = df.getList().get(i).getCity();
                
                //if adding the first item
                if(combocount == 0){
                    startCity.getItems().add(city);
                    endCity.getItems().add(city);
                    combocount++;
                }
                else{
                    //go through the combobox
                    for(int j = 0; j < combocount; j++){
                        //check for duplicates
                        String last = startCity.getItems().get(j);
                        if(city.equals(last)){
                            match = true;
                            break;
                        }
                    }
                    //if no duplicates found, add city to the combobox
                    if(match == false){
                        startCity.getItems().add(city);
                        endCity.getItems().add(city);
                        combocount++;
                    }
                    match = false;
                }
                
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            return;
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            return;
        } catch (NullPointerException ex) {
            errorLabel.setText("DataFetcher palautti arvon: " + df);
            return;
        }
        
        //create some items
        item item1 = new item("lasisärmiö", true, 2, 20, 20, 10);
        item item2 = new item("taikapilleri", false, 0.2, 15, 3, 20);
        item item3 = new item("kumipallo", false, 0.5, 20, 20, 20);
        item item4 = new item("kadonnut koodi", false, 2, 20, 20, 10);
        item item5 = new item("patjaeläin", false, 5, 90, 180, 30);
        
        //add items to the package list
        itemList.add(item1);
        itemList.add(item2);
        itemList.add(item3);
        itemList.add(item4);
        itemList.add(item5);
        
        //add items to the combobox
        items.getItems().add("valitse");
        for(item i : itemList){
            items.getItems().add(i.getName());
        }
    }

    @FXML
    private void classInfo(ActionEvent event) {
        openNewView view = new openNewView("FXMLClassInfo.fxml");
    }

    @FXML
    private void endCityAction(ActionEvent event) {
        addAutomat(endAutomat, endCity.getValue());
    }

    @FXML
    private void startCityAction(ActionEvent event) {
        addAutomat(startAutomat, startCity.getValue());
    }
    
    private void addAutomat(ComboBox<String> c, String city){
        // wrap the list with observable list
        List<String> list = new ArrayList<>();
        ObservableList<String> ob = FXCollections.observableList(list);
        
        //clear the automatlist
        c.setItems(ob);
        
        //add automat
        for(int i=0; i<df.getList().size();i++){
            if(city.equals(df.getList().get(i).getCity())){
                c.getItems().add(df.getList().get(i).getAutomat());
            }
        }
    }

    @FXML
    private void createPackage(ActionEvent event) {
        String name;
        boolean fragile;
        int speed;
        String size;
        double weight, height, width, depth;
        double distance;
        
        String sAutomat = startAutomat.getValue();
        String eAutomat = endAutomat.getValue();
        
        //error detection
        if(sAutomat == null || eAutomat == null){
            errorLabel.setText("Valitse automaatti.");
            return;
        }
        
        //create new package
        if(items.getValue() == null || items.getValue().equals("valitse")){
            try{
                name = nameField.getText();
                fragile = fragileBox.isSelected();
                weight = Double.parseDouble(massField.getText());
                size = sizeField.getText();
                String[] measurements = size.split("\\*");
                height = Double.parseDouble(measurements[0]);
                width = Double.parseDouble(measurements[1]);
                depth = Double.parseDouble(measurements[2]);
            
                //error detection
                if(name.equals("")){
                    errorLabel.setText("Et voi lähettää nimetöntä pakettia");
                    return;
                }
                if(RadioButton1.isSelected()==false && RadioButton2.isSelected()==false && RadioButton3.isSelected()==false){
                    errorLabel.setText("Valitse luokka!");
                    return;
                }
                
                item contents = new item(name, fragile, weight, height, width, depth);
                
                if(RadioButton1.isSelected()==true){
                    if(fragile == true || weight > 5){
                        errorLabel.setText("Paketti ei sovellu lähetettäväksi 1. luokassa.");
                        return;
                    }
                    FirstClass fc1 = new FirstClass(contents, sAutomat, eAutomat);
                    storage.add(fc1);
                }
                else if(RadioButton2.isSelected()==true){
                    if(width > 30 || depth > 30 || height > 30){
                        errorLabel.setText("Paketti ei sovellu lähetettäväksi 2. luokassa.");
                        return;
                    }
                    SecondClass fc2 = new SecondClass(contents, sAutomat, eAutomat);
                    storage.add(fc2);
                }
                else if(RadioButton3.isSelected()==true){
                    if(fragile == true || weight < 5){
                        errorLabel.setText("Paketti ei sovellu lähetettäväksi 3. luokassa.");
                        return;
                    }
                    if(weight < 20 || width < 50 || depth < 50 || height < 50){
                        errorLabel.setText("Pienen paketin lähettäminen omalla vastuulla.");
                    }
                    ThirdClass fc3 = new ThirdClass(contents, sAutomat, eAutomat);
                    storage.add(fc3);
                }
                //clear text and add created item to combobox
                errorLabel.setText(null);
                nameField.setText("");
                sizeField.setText("");
                massField.setText("");
                itemList.add(contents);
                items.getItems().add(name);
            }
            catch(ArrayIndexOutOfBoundsException | NumberFormatException ex){
                errorLabel.setText("Tarkista, että tekstikentät on täytetty oikein!");
            }
        }
        //get from itemlist
        else{
            String itemName = items.getValue();
            item contents = null;
            
            for(item i : itemList){
                if(itemName.equals(i.getName())){
                    contents = i;
                }
            }
            //error detection
            if(contents==null){
                errorLabel.setText("error");
                return;
            }
            //add variables
            fragile = contents.isFragile();
            weight = contents.getWeight();
            width = contents.getWidth();
            depth = contents.getDepth();
            height = contents.getHeight();
            
            if(RadioButton1.isSelected()==true){
                if(fragile == true || weight > 5){
                    errorLabel.setText("Paketti ei sovellu lähetettäväksi 1. luokassa.");
                    return;
                }
                FirstClass fc1 = new FirstClass(contents, sAutomat, eAutomat);
                storage.add(fc1);
            }
            else if(RadioButton2.isSelected()==true){
                if(width > 30 || depth > 30 || height > 30){
                    errorLabel.setText("Paketti ei sovellu lähetettäväksi 2. luokassa.");
                    return;
                }
                SecondClass fc2 = new SecondClass(contents, sAutomat, eAutomat);
                storage.add(fc2);
            }
            else if(RadioButton3.isSelected()==true){
                if(fragile == true || weight < 5){
                    errorLabel.setText("Paketti ei sovellu lähetettäväksi 3. luokassa.");
                    return;
                }
                if(weight < 20 || width < 50 || depth < 50 || height < 50){
                    errorLabel.setText("Pienen paketin lähettäminen omalla vastuulla.");
                }
                ThirdClass fc3 = new ThirdClass(contents, sAutomat, eAutomat);
                storage.add(fc3);
            }
            System.out.println("Wrapping item named " + contents.getName());
        }
    }

    @FXML
    private void closeButtonAction(ActionEvent event) {
        // makes a handle
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // closes the view
        stage.close();
    }
}